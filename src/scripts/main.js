$(document).ready(function() {
    if ($(window).width() <= 640) {
        callModal('.ico-link');
        callModal('.btn--share');
    };

    function callModal(element) {
        $(element).click(function() {
            $('.modal').addClass('active');
            $('body').addClass('modal-bg');
        });
    }
    $('.modal .ico-close').click(function() {
        $('.modal').removeClass('active');
        $('body').removeClass('modal-bg');
    });
})